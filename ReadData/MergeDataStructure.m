function Extract=MergeDataStructure(S)

Kinematics_field={};
Kinetics_field={};
A=load(S.files{1});
Extract=A.Extract;
F.name=fieldnames(Extract);
for j=1:length(F.name)
    if iscell(Extract.(F.name{j}))
        F.cell(j)=1;
    else
        F.cell(j)=0;
    end
    if strcmp(F.name{j},'Data')
        if isfield(Extract.Data,'Kinematics')==1
            Kinematics_field=fieldnames(Extract.Data.Kinematics);
        end
        if isfield(Extract.Data,'Kinetics')==1
            Kinetics_field=fieldnames(Extract.Data.Kinetics);
        end
    end
end
for i=2:length(S.files)
    disp(['Merging files ' num2str(i) '/' num2str(length(S.files))]);
    B=load(S.files{i});
    for j=1:length(F.name)
        if F.cell(j)==1
            Extract.(F.name{j})=[Extract.(F.name{j}) B.Extract.(F.name{j})];
        end
        switch F.name{j}
            case 'Cycle'
                Extract.(F.name{j}).L=[Extract.(F.name{j}).L;B.Extract.(F.name{j}).L];
                Extract.(F.name{j}).R=[Extract.(F.name{j}).R;B.Extract.(F.name{j}).R];
            case  'Event'
                Extract.(F.name{j}).Right_Foot_Strike=[Extract.(F.name{j}).Right_Foot_Strike;B.Extract.(F.name{j}).Right_Foot_Strike];
                Extract.(F.name{j}).Left_Foot_Strike=[Extract.(F.name{j}).Left_Foot_Strike;B.Extract.(F.name{j}).Left_Foot_Strike];
                Extract.(F.name{j}).Right_Foot_Off=[Extract.(F.name{j}).Right_Foot_Off;B.Extract.(F.name{j}).Right_Foot_Off];
                Extract.(F.name{j}).Left_Foot_Off=[Extract.(F.name{j}).Left_Foot_Strike;B.Extract.(F.name{j}).Left_Foot_Off];
            case  'Data'
                if isempty(Kinematics_field)==0
                    for Kn=1:length(Kinematics_field)
                        Extract.Data.Kinematics.(Kinematics_field{Kn})=[Extract.Data.Kinematics.(Kinematics_field{Kn});B.Extract.Data.Kinematics.(Kinematics_field{Kn})];
                    end
                end
                if isempty(Kinetics_field)==0
                    for Kn=1:length(Kinetics_field)
                        Extract.Data.Kinetics.(Kinetics_field{Kn})=[Extract.Data.Kinetics.(Kinetics_field{Kn});B.Extract.Data.Kinetics.(Kinetics_field{Kn})];
                    end
                end
        end
    end
end
end
