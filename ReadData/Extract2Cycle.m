function [Mean, Cycle]=Extract2Cycle(Extract)

disp('Preparing data to display...');
Cycle=struct();
PointsByCycle=101;
kinematic_field=fieldnames(Extract.Data.Kinematics);
if isfield(Extract.Data,'Kinetics')
    kinetic_field=fieldnames(Extract.Data.Kinetics);
else
    kinetic_field='';
end
Field2Extract={'Condition','Subject','Subject_Session','Filename','Folder','Session','Enf_file'};

ln=0;lr=0;
for j=1:length(kinematic_field)
    if strcmp(kinematic_field{j}(1),'L')
        ln=ln+1;
        left_kinematic_field{ln}=kinematic_field{j};
    end
    if strcmp(kinematic_field{j}(1),'R')
        lr=lr+1;
        right_kinematic_field{lr}=kinematic_field{j};
    end
end


ln=0;lr=0;
for j=1:length(kinetic_field)
    if strcmp(kinetic_field{j}(1),'L')
        ln=ln+1;
        left_kinetic_field{ln}=kinetic_field{j};
    end
    if strcmp(kinetic_field{j}(1),'R')
        lr=lr+1;
        right_kinetic_field{lr}=kinetic_field{j};
    end
end



% Kinematic cycles - Left
disp('Preparing Left cycle');
R=[];C=[];R2=[];IX=[];C=[];C2=[];CycleValue2=[];CycleValue=[];
[R,C,V] = find(Extract.Cycle.L>0);
for i=1:length(R)
    CycleValue(i)=Extract.Cycle.L(R(i),C(i));
end
[R2,IX]=sort(R);
C2=C(IX);
CycleValue2=CycleValue(IX);
n=0;
for i=1:length(R2)
    S=[];F=[];
    S=Extract.Event.Left_Foot_Strike(R2(i),C2(i));
    F=Extract.Event.Left_Foot_Strike(R2(i),C2(i)+1);
    if isnan(F)==0
        n=n+1;
        for Fi=1:length(Field2Extract)
            Cycle.L.(Field2Extract{Fi}){n}=Extract.(Field2Extract{Fi}){R2(i)};
        end
        Cycle.L.ID{n}=[Cycle.L.Subject{n} ',' Cycle.L.Session{n} ',' Cycle.L.Filename{n} ',' Cycle.L.Enf_file{n} ',' Cycle.L.Folder{n} ',Left,' num2str(C2(i)) ',' num2str(S) ',' num2str(F) ',' num2str(n)];
        for j=1:length(left_kinematic_field)
            D=[];D2=[];
            
            D=Extract.Data.Kinematics.(left_kinematic_field{j})(R2(i),S:F);
            
            if sum(D)==0
                Cycle.L.Data.(left_kinematic_field{j})(n,1:PointsByCycle)=NaN;
            else
                D2=interpol(D,PointsByCycle);
                Cycle.L.Data.(left_kinematic_field{j})(n,:)=D2;
            end
        end
        if isempty(kinetic_field)==0
            for j=1:length(left_kinetic_field)
                D=[];D2=[];
                if CycleValue2(i)==2
                    D=Extract.Data.Kinetics.(left_kinetic_field{j})(R2(i),S:F);
                    if max(D)>10
                        D=D/1000; % to correct scale difference between vicon and qualysis Vicon express in Nmm, Qualisys expressed in Nm
                    end
                    if sum(D)==0
                        Cycle.L.Data.(left_kinetic_field{j})(n,1:PointsByCycle)=NaN;
                    else
                        D2=interpol(D,PointsByCycle);
                        Cycle.L.Data.(left_kinetic_field{j})(n,:)=D2;
                    end
                else
                    Cycle.L.Data.(left_kinetic_field{j})(n,1:PointsByCycle)=NaN;
                end
            end
        end
    end
    
end


% Kinematic cycles - Right
disp('Preparing Right cycle');
R=[];C=[];R2=[];IX=[];C=[];C2=[];CycleValue2=[];CycleValue=[];
[R,C,V] = find(Extract.Cycle.R>0);
for i=1:length(R)
    CycleValue(i)=Extract.Cycle.R(R(i),C(i));
end
[R2,IX]=sort(R);
C2=C(IX);
CycleValue2=CycleValue(IX);
n=0;
for i=1:length(R2)
    S=[];F=[];
    S=Extract.Event.Right_Foot_Strike(R2(i),C2(i));
    F=Extract.Event.Right_Foot_Strike(R2(i),C2(i)+1);
    if isnan(F)==0
        n=n+1;
        for Fi=1:length(Field2Extract)
            Cycle.R.(Field2Extract{Fi}){n}=Extract.(Field2Extract{Fi}){R2(i)};
        end
        Cycle.R.ID{n}=[Cycle.R.Subject{n} ',' Cycle.R.Session{n} ',' Cycle.R.Filename{n} ','  Cycle.R.Enf_file{n} ','  Cycle.R.Folder{n} ',Right,' num2str(C2(i)) ',' num2str(S) ',' num2str(F) ',' num2str(n)];
        
        for j=1:length(right_kinematic_field)
            D=[];D2=[];
            D=Extract.Data.Kinematics.(right_kinematic_field{j})(R2(i),S:F);
            if sum(D)==0
                Cycle.R.Data.(right_kinematic_field{j})(n,1:PointsByCycle)=NaN;
            else
                D2=interpol(D,PointsByCycle);
                Cycle.R.Data.(right_kinematic_field{j})(n,:)=D2;
            end
        end
        if isempty(kinetic_field)==0
            for j=1:length(right_kinetic_field)
                D=[];D2=[];
                if CycleValue2(i)==2
                    D=Extract.Data.Kinetics.(right_kinetic_field{j})(R2(i),S:F);
                    if max(D)>10
                        D=D/1000; % to correct scale difference between vicon and qualysis Vicon express in Nmm, Qualisys expressed in Nm
                    end
                    if sum(D)==0
                        Cycle.R.Data.(right_kinetic_field{j})(n,1:PointsByCycle)=NaN;
                    else
                        D2=interpol(D,PointsByCycle);
                        Cycle.R.Data.(right_kinetic_field{j})(n,:)=D2;
                    end
                else
                    Cycle.R.Data.(right_kinetic_field{j})(n,1:PointsByCycle)=NaN;
                end
            end
        end
    end
end

disp('Preparing Mean by subject at Left side');
A=[];B=[];C=[];
Sub=unique(Cycle.L.Subject_Session);
Mean.Sub.L.Subject_Session=Sub;
Cond=unique(Cycle.L.Condition);
Mean.Cond.L.Condition=Cond;
Mean.Subject.Name=Sub;
Mean.Condition.Name=Cond;
left_data_field=fieldnames(Cycle.L.Data);
for i=1:length(Sub)
    for j=1:length(left_data_field)
        A=strcmp(Cycle.L.Subject_Session,Sub(i));
        B=find(A==1);
        Mean.Subject.L.Data.(left_data_field{j})(i,:)=nanmean(Cycle.L.Data.(left_data_field{j})(B,:));
    end
end

disp('Preparing Mean by Condition at Left side');
for i=1:length(Cond)
    for j=1:length(left_data_field)
        A=strcmp(Cycle.L.Condition,Cond(i));
        B=find(A==1);
        Mean.Condition.L.Data.(left_data_field{j})(i,:)=nanmean(Cycle.L.Data.(left_data_field{j})(B,:));
    end
end
n=0;

disp('Preparing Mean by Subject - Condition at Left side');
for i=1:length(Sub)
    for k=1:length(Cond)
        n=n+1;
        Mean.Subject_Condition.Name{n}=[Sub{i} '_' Cond{k}];
        for j=1:length(left_data_field)
            A=strcmp(Cycle.L.Subject_Session,Sub(i));
            B=strcmp(Cycle.L.Condition,Cond(k));
            C=find(A+B==2);
            Mean.Subject_Condition.L.Data.(left_data_field{j})(n,:)=nanmean(Cycle.L.Data.(left_data_field{j})(C,:));
        end
    end
end

disp('Preparing Mean by subject at Right side');
A=[];B=[];C=[];
Sub=unique(Cycle.R.Subject_Session);
Mean.Sub.R.Subject_Session=Sub;
Cond=unique(Cycle.R.Condition);
Mean.Cond.R.Condition=Cond;
right_data_field=fieldnames(Cycle.R.Data);
for i=1:length(Sub)
    for j=1:length(right_data_field)
        A=strcmp(Cycle.R.Subject_Session,Sub(i));
        B=find(A==1);
        Mean.Subject.R.Data.(right_data_field{j})(i,:)=nanmean(Cycle.R.Data.(right_data_field{j})(B,:));
    end
end

disp('Preparing Mean by Condition at Right side');
for i=1:length(Cond)
    for j=1:length(right_data_field)
        A=strcmp(Cycle.R.Condition,Cond(i));
        B=find(A==1);
        Mean.Condition.R.Data.(right_data_field{j})(i,:)=nanmean(Cycle.R.Data.(right_data_field{j})(B,:));
    end
end
n=0;

disp('Preparing Mean by Subject - Condition at Right side');
for i=1:length(Sub)
    for k=1:length(Cond)
        n=n+1;
        Mean.Subject_Condition.Name{n}=[Sub{i} '_' Cond{k}];
        for j=1:length(right_data_field)
            A=strcmp(Cycle.R.Subject_Session,Sub(i));
            B=strcmp(Cycle.R.Condition,Cond(k));
            C=find(A+B==2);
            Mean.Subject_Condition.R.Data.(right_data_field{j})(n,:)=nanmean(Cycle.R.Data.(right_data_field{j})(C,:));
        end
    end
end




%
%  A=Cycle.L.Data.Kinematics.LKneeAngles1;
%  B=Cycle.R.Data.Kinematics.RKneeAngles1;%(strcmp(Cycle.Condition,'GNNNN'),:);
%  % B=Cycle.Subject(strcmp(Cycle.Condition,'GNNNN'));
%   %B=Cycle.Data.Kinematics.LKneeAngles1(strcmp(Cycle.Subject,'EXOB 021') & strcmp(Cycle.Condition,'GNENN'),:);
%  %figure;plot(A','DisplayName',B)
%  figure;plot(A','r');hold on; plot(B','b');
%

% % Kinematic cycles - Right
% R=[];C=[];R2=[];IX=[];C2=[];
% [R,C] = find(Extract.Cycle.R>0);
% [R2,IX]=sort(R);
% C2=C(IX);
% n=0;
% for i=1:length(R2)
%     for j=1:length(kinematic_field)
%         S=[];F=[];D=[];D2=[];
%         S=Extract.Event.Left_Foot_Strike(R2(i),C2(i));
%         F=Extract.Event.Left_Foot_Strike(R2(i),C2(i)+1);
%         D=Extract.Data.Kinematics.(kinematic_field{j})(R2(i),S:F);
%         if sum(D)==0
%             Cycle.Data.Kinematics.(kinematic_field{j})(n,1:PointsByCycle)=NaN;
%         else
%         D2=interpol(D,PointsByCycle);
%         n=n+1;
%         Cycle.Data.Kinematics.(kinematic_field{j})(n,:)=D2;
%         end
%     end
% end