function Param = ParseC3Dparameter(file)
% parse display text file and store output into a Pgraph strucure array

i=0;
delimit=',';
fid = fopen(file,'r');
while feof(fid) == 0
    tline = fgetl(fid);
    if tline(1) ~= '/'
        e=strfind(tline,',');
        i=i+1;
        [Param(i).Name Param(i).Type Param(i).VarC3D Param(i).Plan Param(i).Side]  =strread(tline,'%s %s %s %f %s','delimiter',delimit);
    end
end
fclose(fid);
F=fieldnames(Param);

for i=1:length(Param)
    for j=1:length(F)
        if iscell(Param(i).(F{j}))==1
            Param(i).(F{j})=char(Param(i).(F{j}));
        end
    end
end

return;
end
