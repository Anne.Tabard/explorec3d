function  Patient = ParseLibrary(param_file)
% parse library text file and store output into a Patient strucure array

    Patient = struct();
    i = 0;
    delimit = ',';
    fid = fopen(param_file,'r');
    while feof(fid) == 0
        tline = fgetl(fid);
        if tline(1) ~= '/'
            i = i+1;
            [Patient(i).Name Patient(i).Param1 Patient(i).Param2 Patient(i).Param3 Patient(i).Path...
                Patient(i).Session Patient(i).FileType Patient(i).files] = strread(tline,'%s %s %s %s %s %s %s %s','delimiter',delimit);
        end
    end
    fclose(fid);
    for i = 1:length(Patient)
        Patient(i).Name = char(Patient(i).Name);
        Patient(i).Param1 = char(Patient(i).Param1);
        Patient(i).Param2 = char(Patient(i).Param2);
        Patient(i).Param3 = char(Patient(i).Param3);
        Patient(i).Path = char(Patient(i).Path);
        Patient(i).Session = char(Patient(i).Session);
        Patient(i).files = char(Patient(i).files);
        Patient(i).FileType = char(Patient(i).FileType);
        k2 = strfind(Patient(i).files,':');

        if isempty(k2)==1 %cas 'liste de fichiers 2 3 4 10 20'
            a=isstrprop(Patient(i).files,'digit');
            t=0;
            t3=0;
            while t<length(a) % pour le nombre d'�l�ment
                t=t+1;
                t2=0;
                passe=0;
                file3='0';
                if a(t)==1 % si a(t) est un nombre
                    while (a(t)==1) && (t<=length(a))%
                        t2=t2+1;
                        file3(t2)=char(Patient(i).files(t));
                        t=t+1;
                        passe=1;
                        if t>length(a)
                            break
                        end
                    end
                    if passe==1
                        file2=str2double(file3);
                        t3=t3+1;
                        if file2<10
                            Patient(i).file(t3).name=[Patient(i).Session '0' file3 '.' Patient(i).FileType];
                        else
                            Patient(i).file(t3).name=[Patient(i).Session file3 '.' Patient(i).FileType];
                        end
                    end
                end
            end

        else
            deb=str2double([Patient(i).files(1) Patient(i).files(k2-1)]);
            fin=str2double([Patient(i).files(k2+1) Patient(i).files(end)]);
            for a=1:(fin-deb)+1
                file2=deb+a-1;
                if file2<10
                    Patient(i).file(a).name=[Patient(i).Session '0' num2str(file2) '.' Patient(i).FileType];
                else
                    Patient(i).file(a).name=[Patient(i).Session num2str(file2) '.' Patient(i).FileType];
                end
            end
        end
    end
    
    return;
end

