function info=Get_Enf_General(file)
% to complete

fid=fopen(file);
info=struct();
i=1;
while 1
    tline = fgetl(fid);
    if ~ischar(tline),   break,   end
    e=strfind(tline,'=');
    if isempty(e)==0
        if e<length(tline)
            switch tline(1:(e-1))
                case 'RIGHT_CYCLE'
                    v=strfind(tline(e+1:end),',');
                    if strcmp(tline(e+1),'A')==1 % pour les arrets
                        X=strfind(tline,'A');
                        if isempty(X)==0
                            X=[X length(tline)+2];
                            for v2=1:length(X)-1
                                info.RStop(v2)=str2num(tline(X(v2)+1:X(v2+1)-2));
                            end
                        else % cas ou 1 seul cycle
                            info.RStop(1)=str2num(tline(X+1:end));
                        end
                        
                    else % pour les cycles classique
                        
                        if isempty(v)==0
                            if length(tline(e+1:end))>length(v)*2+1 % si lettre dans cycle pour spécifier model valid
                                v=[1 v+1];
                                for v2=1:length(v)
                                    info.RCycle(v2)=str2num(tline(e+v(v2)));
                                    if length(tline)>=e+1+v(v2) % pour le dernier element
                                        if strcmp(',',tline(e+1+v(v2)))==0  % pour exclure quand je tombe sur une virgule
                                            info.RCycleModel(v2)=tline(e+1+v(v2));
                                        else
                                            info.RCycleModel(v2)='F';
                                        end
                                    else
                                        info.RCycleModel(v2)='F';
                                    end
                                end
                                
                            else  % si  cycle pour corps entier
                                v=[1 v+1];
                                for v2=1:length(v)
                                    info.RCycle(v2)=str2num(tline(e+v(v2)));
                                    info.RCycleModel(v2)='F';
                                end
                            end
                        else % cas ou 1 seul cycle
                            if length(tline(e+1:end))==1
                                info.RCycle(1)=str2num(tline(end));
                                info.RCycleModel(1)='F';
                            end
                            
                            if length(tline(e+1:end))==2
                                info.RCycle(1)=str2num(tline(end-1));
                                info.RCycleModel(1)=(tline(end));
                            end
                        end
                    end
                case 'LEFT_CYCLE'
                    v=strfind(tline(e+1:end),',');
                    if strcmp(tline(e+1),'A')==1 % pour les arrets
                        X=strfind(tline,'A');
                        if isempty(X)==0
                            X=[X length(tline)+2];
                            for v2=1:length(X)-1
                                info.LStop(v2)=str2num(tline(X(v2)+1:X(v2+1)-2));
                            end
                        else % cas ou 1 seul cycle
                            info.LStop(1)=str2num(tline(X+1:end));
                        end
                        
                    else % pour les cycles classique
                        
                        if isempty(v)==0
                            if length(tline(e+1:end))>length(v)*2+1 % si lettre dans cycle pour spécifier model valid
                                v=[1 v+1];
                                for v2=1:length(v)
                                    info.LCycle(v2)=str2num(tline(e+v(v2)));
                                    if length(tline)>=e+1+v(v2) % pour le dernier element
                                        if strcmp(',',tline(e+1+v(v2)))==0  % pour exclure quand je tombe sur une virgule
                                            info.LCycleModel(v2)=tline(e+1+v(v2));
                                        else
                                            info.LCycleModel(v2)='F';
                                        end
                                    else
                                        info.LCycleModel(v2)='F';
                                    end
                                end
                                
                            else  % si  cycle pour corps entier
                                v=[1 v+1];
                                for v2=1:length(v)
                                    info.LCycle(v2)=str2num(tline(e+v(v2)));
                                    info.LCycleModel(v2)='F';
                                end
                            end
                        else % cas ou 1 seul cycle
                            if length(tline(e+1:end))==1
                                info.LCycle(1)=str2num(tline(end));
                                info.LCycleModel(1)='F';
                            end
                            
                            if length(tline(e+1:end))==2
                                info.LCycle(1)=str2num(tline(end-1));
                                info.LCycleModel(1)=(tline(end));
                            end
                        end
                    end
                case 'CREATIONDATEANDTIME'
                    N=findstr(',',tline);
                    info.date.year=str2num(tline(N(1)-4:N(1)-1));
                    info.date.month=str2num(tline(N(1)+1:N(2)-1));
                    info.date.day=str2num(tline(N(2)+1:N(3)-1));
                    info.date.hour=str2num(tline(N(3)+1:N(4)-1));
                    info.date.min=str2num(tline(N(4)+1:N(5)-1));
                    info.date.sec=str2num(tline(N(5)+1:end));                  
                case 'CREATION_DATE'
                    N=findstr(',',tline);
                    info.date.year=str2num(tline(15:18));
                    info.date.month=str2num(tline(19:20));
                    info.date.day=str2num(tline(21:22));
                    tline2=tline(1:(e-1));
                    tline2=strrep(tline2,' ','');
                    tline2=strrep(tline2,'$','');
                    info.(tline2)=char(tline(e+1:end));
                case 'CREATION_TIME'
                    N=findstr(':',tline);
                    info.date.hour=str2num(tline(N(1)-2:N(1)-1));
                    info.date.min=str2num(tline(N(1)+1:N(2)-1));
                    info.date.sec=str2num(tline(N(2)+1:end));
                    tline2=tline(1:(e-1));
                    tline2=strrep(tline2,' ','');
                    tline2=strrep(tline2,'$','');
                    info.(tline2)=char(tline(e+1:end));
                    %                 case 'DESCRIPTION'
                    %                     tline2=tline(1:(e-1));
                    %                     tline2=strrep(tline2,' ','');
                    %                     tline2=strrep(tline2,'$','');
                    %                     info.NOTES=char(tline(e+1:end));
                case 'COMMENTS'
                    tline2=tline(1:(e-1));
                    tline2=strrep(tline2,' ','');
                    tline2=strrep(tline2,'$','');
                    info.DESCRIPTION=char(tline(e+1:end));
                    tline2=tline(1:(e-1));
                    tline2=strrep(tline2,' ','');
                    tline2=strrep(tline2,'$','');
                    info.(tline2)=char(tline(e+1:end));
                case 'DATE_OF_BIRTH'
                    tline2=tline(1:(e-1));
                    tline2=strrep(tline2,' ','');
                    tline2=strrep(tline2,'$','');
                    b=char(tline(e+1:end));
                    info.BIRTH=[b(end-1:end) '.' b(end-3:end-2) '.' b(1:4)];
                otherwise
                    tline2=tline(1:(e-1));
                    tline2=strrep(tline2,' ','');
                    tline2=strrep(tline2,'$','');
                    info.(tline2)=char(tline(e+1:end));
            end
        end
    end
end
fclose(fid);
