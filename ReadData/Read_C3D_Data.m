function [Extract]=Read_C3D_Data(List,Param, Pcalcul, S)
%filter='';
Extract=[];
analog=0;
cyclemax=20;
eventmax=50;
datamax=5000;
nb_files=500;
% Param(1).Name='LKneeAngles1';
% Param(1).VarC3D='LKneeAngles';
% Param(1).Plan=1;
% Param(1).Type='Kinematics';
% Param(1).Side='L';

%A=subdir([folder filter]);
a=0;
for i=1:length(List)
    for j=1:length(List(i).file)
        a=a+1;
        A(a).name=[List(i).Path List(i).file(j).name];
    end
end
Event_name={'Right_Foot_Strike','Left_Foot_Strike','Right_Foot_Off','Left_Foot_Off'};

disp(['**** ' num2str(length(A)) ' files to read ****']);

n=0;
S.n=0;
for i=1:length(A)
    [pathstr, name, ext] = fileparts(A(i).name);
    if length(name)==22;
        if strcmp(name(end-1:end),'.1')==0 %|| strcmp(name(end-1:end),'.3')==0
            C3D = Get_C3D_BTK([pathstr '\'],[name ext],analog);
            %% compute
            for Calc = 1:length(Pcalcul)
                ToCompute = char(Pcalcul(Calc).Programme);
                if(~strcmp(char(Pcalcul(Calc).Option1),'0'))
                    if(strcmp(char(Pcalcul(Calc).Option1),'Param'))
                        C3D = eval([ToCompute '(C3D, char(File(i).Param1), 1)']);
                    else
                        C3D = eval([ToCompute '(C3D, char(Pcalcul(Calc).Option1), 1)']);
                    end
                else
                    C3D = eval([ToCompute '(C3D, 1)']);
                end
            end
            
            %%
            btkDeleteAcquisition(C3D.acq);
            disp(['Reading ' num2str(i) '/' num2str(length(A)) ' - ' A(i).name]);
            n=n+1;
            
            % valid cycle for kinematic/kinetic
            Extract.Cycle.L(n,1:cyclemax)=NaN;
            Extract.Cycle.R(n,1:cyclemax)=NaN;
            D=[];
            if isfield(C3D,'LCycle')
                D=C3D.LCycle;
                if length(D)<=cyclemax
                    Extract.Cycle.L(n,1:length(D))= C3D.LCycle;
                else
                    Extract.Cycle.L(n,1:cyclemax)= C3D.LCycle(1:cyclemax);
                    disp('Number of Lcycle superior to max');
                end
            end
            D=[];
            if isfield(C3D,'RCycle')
                D=C3D.RCycle;
                if length(D)<=cyclemax
                    Extract.Cycle.R(n,1:length(D))= C3D.RCycle;
                else
                    Extract.Cycle.R(n,1:cyclemax)= C3D.RCycle(1:cyclemax);
                    disp('Number of Rcycle superior to max');
                end
            end
            
            % event frame
            D=[];
            for j=1:length(Event_name)
                Extract.Event.(Event_name{j})(n,1:eventmax)=NaN;
                if isfield(C3D,'EventFrame')
                    if isfield(C3D.EventFrame,Event_name{j})
                        D=C3D.EventFrame.(Event_name{j});
                        if length(D)<=eventmax
                            Extract.Event.(Event_name{j})(n,1:length(D))=D;
                        else
                            Extract.Event.(Event_name{j})(n,1:eventmax)=D(1:eventmax);
                            disp('Number of events superior to max');
                        end
                    end
                end
            end
            
            % parameters/data
            D=[];
            for j=1:length(Param)
                Extract.Data.(Param(j).Type).(Param(j).Name)(n,1:datamax)=NaN;
                if isfield(C3D.data,Param(j).VarC3D)
                    D=C3D.data.(Param(j).VarC3D)(:,Param(j).Plan);
                    if length(D)<=datamax
                        Extract.Data.(Param(j).Type).(Param(j).Name)(n,1:length(D))=D;
                    else
                        Extract.Data.(Param(j).Type).(Param(j).Name)(n,1:datamax)=D(1:datamax);
                        disp('size of data superior to max');
                    end
                end
            end
            
            %  name
            Extract.Condition{n}= name(10:14);
            Extract.Subject{n}=C3D.Patient_Name;
            Extract.Session{n}=C3D.Session_Name;
            Extract.Filename{n}=name;
            Extract.Folder{n}=pathstr;
            if isfield(C3D,'Enf_file')==1
                Extract.Enf_file{n}=C3D.Enf_file;
            else
                Extract.Enf_file{n}='Not found';
            end
            
            %             if length(C3D.Patient_Name)>8
            %                 if length(C3D.Session_Name)>8
            %                     Extract.Subject_Session{n}=[C3D.Patient_Name(1:8) '_' C3D.Session_Name(1:8)];
            %                 end
            %             else
            if length(C3D.Session_Name)>7
                Extract.Subject_Session{n}=[C3D.Patient_Name '_' C3D.Session_Name(1:8)];
            else
                Extract.Subject_Session{n}=[C3D.Patient_Name '_' C3D.Session_Name];
            end
            %             end
            
            Extract.Param=Param;
            if n>=nb_files
                S.n=S.n+1;
                Fname=[S.folder2save '\' S.file2save '_' num2str((S.n-1)*n+1) '_' num2str(S.n*n) '.mat'];
                S.files{S.n}=Fname;
                S.N(S.n)=S.n*n;
                save(Fname,'Extract');
                disp(['Saving... ' Fname]);
                n=0;
                Extract=struct();
            end
            
        end
    end
    if i==length(A)
        t=datestr(now,'yyyy-mm-dd_HHMM');
        if S.n>0
            if  n>0
                S.n=S.n+1;
                S.N(S.n)=S.n;
                Fname=[S.folder2save '\' S.file2save '_' num2str(S.N(S.n-1)+1) '_' num2str(S.N(S.n-1)+n) '.mat'];
                S.files{S.n}=Fname;
                save(Fname,'Extract');
                save([S.folder2save '\S_data.mat'],'S');
                disp(['Saving... ' Fname]);
            end
            Fname=[S.folder2save '\' S.file2save '_' t '.mat'];
            disp('Merging saved files...')
            Extract=MergeDataStructure(S);
            %                         Field=fieldnames(Extract);
            %                         for ff=1:length(Field)
            %                             if strcmp(Field(ff),'data')==1
            %                             else
            %                                 savelist={savelist Field{ff}}
            %                             end
            %                         end
            save(Fname,'Extract','-v7.3');
            %            save(Fname,savelist{:},'-v7.3');
            
            
        end
        if  S.n==0;
            Fname=[S.folder2save '\' S.file2save '_' t '.mat'];
            save(Fname,'Extract','-v7.3');
            disp(['Saving... ' Fname]);
        end
    end
end
