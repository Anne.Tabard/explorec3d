function E=MergeDataStructure2(S)
%load('D:\Programme\ExploreC3D\Data\S_data.mat');

for i=1:length(S.files)
    disp(['Merging files ' num2str(i) '/' num2str(length(S.files))]);
    B=load(S.files{i});
    E(i)=B.Extract;
end
