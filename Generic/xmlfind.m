function [output] = xmlfind(XML_file,XPath_query)
% find specific content within XML document and convert it to matlab object
% the XMLFIND function performs an XPath query on an XML document in order
% to get to the specific field. It then convert the string field to a
% matlab object, according to the specified field type:
% - library --> structure array
% - struct --> structure
% - cell --> cell array
% - char --> character array
% - double --> number or matrix

    % Import the XPath classes
    import javax.xml.xpath.*

    % Construct the DOM.
    DOM = xmlread(XML_file);

    % Create an XPath expression.
    factory = XPathFactory.newInstance;
    xpath = factory.newXPath;
    expression = xpath.compile(XPath_query);

    % Apply the expression to the DOM to retrieve the nodes.
    nodeList = expression.evaluate(DOM,XPathConstants.NODESET);
    
    % Throw exception if no match.
    if(nodeList.getLength == 0)
        unmatched_path = MException('xmlfind:UnmatchedPath',['Error: the ''',XPath_query,''' path does not match with any element of the XML document']);
        throw(unmatched_path);
    % Throw exception if match is not unique.
    elseif(nodeList.getLength > 1)
        several_matches = MException('xmlfind:SeveralMatches','Error: the provided path does match with more than one element of the XML document');
        throw(several_matches);
    % Retrive the Node if the match exists and is unique.
    else
        node = nodeList.item(0);
        output = extractNode(node);
    end
    
    % extractNode function
    function [node_value] = extractNode(node)
        
        % Get Node Name and Type (specified as an attribute)
        name = char(node.getNodeName);
        type = char(node.getAttribute('type'));
        
        switch type 
            
            case 'library' % If the Node is specified to be a library (structure array)
                % Get the Node Children
                children = node.getChildNodes;
                child_node = node.getFirstChild;
                
                % Initialize a structure to contain the Children contents
                node_value = struct();
                
                % Retrieving only elements Node and storing their names into the row
                index = 1;
                for i = 0:children.getLength - 1
                    if(child_node.getNodeType == 1)
                        if(index == 1)
                            reference_node_name = char(child_node.getNodeName);
                            node_value = extractNode(child_node);
                            index = index + 1;
                        else
                            if(strcmp(char(child_node.getNodeName),reference_node_name))
                                node_value(index) = extractNode(child_node);
                                index = index + 1;
                            else
                                incompatible_child = MException('xmlfind:incompatible_child',['Error: the ',name,' row contains incompatible children']);
                                throw(incompatible_child);
                            end
                        end
                    end
                    child_node = child_node.getNextSibling;
                end
            
            case 'struct' % If the Node is specified to be a structure
                % Get the Node Children
                children = node.getChildNodes;
                child_node = node.getFirstChild;      
                
                % Initialize a structure to contain the Children contents
                node_value = struct();
                
                % Retrieving only elements Node and storing their names
                % and values into the structure
                for i = 0:children.getLength - 1
                    if(child_node.getNodeType == 1)
                        child_node_name = char(child_node.getNodeName);
                        child_node_value = extractNode(child_node);
                        node_value.(child_node_name) = child_node_value;
                    end
                    child_node = child_node.getNextSibling;
                end
                
            case 'cell' % If the Node is specified to be a cell array
                if(isempty(node.getFirstChild))
                    node_value = {};
                else
                    node_value = eval(node.getFirstChild.getNodeValue);
                end
                
            case 'char' % If the Node is specified to be a character
                if(isempty(node.getFirstChild))
                    node_value = '';
                else
                    node_value = char(node.getFirstChild.getNodeValue);
                end
            case 'double' % If the Node is specified to be a number 
                if(isempty(node.getFirstChild))
                    node_value = [];
                else
                    node_value = eval(node.getFirstChild.getNodeValue);
                end
            case '' % If the Node Type does not exist
                no_type = MException('xmlfind:NoType',...
                    ['Error: there is no type specified in the attribute of the ''',name,''' element you are trying to retrieve']);
                throw(no_type);
                
            otherwise % If the Node Type is not recognized
                unknown_type = MException('xmlfind:UnknownType',...
                    ['Error: the ''',type,''' type specified in the attribute of the ''',name,''' element you are trying to retrieve is unknown']);
                throw(unknown_type);
        end
        
        return;
    end
    
    return;
end