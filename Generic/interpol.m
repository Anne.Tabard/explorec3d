function varout=interpol(varin,taille)
% normalize data according to gait cycle
%
% Input: varin (data to normalize) and number of point for interpolating (taille)
% Output: varout (normalized data)

varout=interp1(1:length(varin),varin,1:(length(varin)-1)/(taille-1):length(varin));
