function [output_parameter] = GetParameterExploreC3DPref(parameter_subpath)
% get specific field of the parameters XML file using the XMLFIND function

   % output_parameter = xmlfind('D:\Programme\ExploreC3D - sinergia\preference.xml',strcat('/parameters/',parameter_subpath));
    output_parameter = xmlfind('D:\ANNE_D\PROJETS\SINERGIA\Code\ExploreC3D - sinergia\preference.xml',strcat('/parameters/',parameter_subpath));
    return;
end