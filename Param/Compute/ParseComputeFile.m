function  Pcalcul = ParseComputeFile(file)
% parse compute text file and store output into a Pcalcul strucure array

    Pcalcul=struct();
    i=0;
    delimit=',';
    fid = fopen(file,'r');
    while feof(fid) == 0
        tline = fgetl(fid);
        if tline(1) ~= '/'
            i=i+1;
            [Pcalcul(i).Programme Pcalcul(i).Option1]=strread(tline,'%s %s','delimiter',delimit); 
        end
    end
    status = fclose(fid);

    return;
end
