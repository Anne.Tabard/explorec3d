/affiche,nom,plan,type,titre,axe y-,y+, ymin,ymay,increment,unit,figure,subplot n,subplot m,subplot position
/
/Angles
1,ThoraxAngles,1,T,Angle,Thorax Anteversion,Posterieur,Anterieur,-30,30,20,�,1,5,3,1
1,ThoraxAngles,2,T,Angle,Thorax Obliquite,Bas,Haut,-30,30,20,�,1,5,3,2
1,ThoraxAngles,3,T,Angle,Thorax Rotation,Externe,Interne,-30,30,20,�,1,5,3,3
1,PelvisAngles,1,M,Angle,Pelvis Anteversion,Posterieur,Anterieur,-30,30,20,�,1,5,3,4
1,PelvisAngles,2,M,Angle,Pelvis Obliquite,Bas,Haut,-30,30,20,�,1,5,3,5
1,PelvisAngles,3,M,Angle,Pelvis Rotation,Externe,Interne,-30,30,20,�,1,5,3,6
1,HipAngles,1,M,Angle,Hanche Flexion-Extension,Extension,Flexion,-20,80,20,�,1,5,3,7
1,HipAngles,2,M,Angle,Hanche Abduction-Adduction,Abduction,Adduction,-30,30,20,�,1,5,3,8
1,HipAngles,3,M,Angle,Hanche Rotation,Externe,Interne,-30,30,20,�,1,5,3,9  
1,KneeAngles,1,M,Angle,Genou Flexion-Extension,Extension,Flexion,-20,80,20,�,1,5,3,10
1,KneeAngles,2,M,Angle,Genou Valgus-Varus,Valgus,Varus,-30,30,20,�,1,5,3,11
1,KneeAngles,3,M,Angle,Genou Rotation,Externe,Interne,-30,30,20,�,1,5,3,12
1,AnkleAngles,1,M,Angle,Cheville Flexion Plantaire-Dorsale,Plantaire,Dorsale,-30,30,20,�,1,5,3,13
1,FootProgressAngles,3,M,Angle,Angle de Progression du pas,Externe,Interne,-30,30,20,�,1,5,3,15
/1,AnkleAngles,3,M,Angle,Cheville Rotation,Externe,Interne,-30,30,20,�,1,5,3,15
/1,AnkleISBAngles,2,M,Angle,Cheville Abduction/Adduction ISB,Abduction,Adduction,-30,30,20,�,1,5,3,14
/1,AnkleISBAngles2,2,M,Angle,Cheville Eversion/Inversion,Eversion,Inversion,-30,30,20,�,1,5,3,12
/1,AnkleAngles,2,M,Angle,Cheville Abduction/Adduction ISB,Abduction,Adduction,-30,30,20,�,1,5,3,12