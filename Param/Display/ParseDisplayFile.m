function Pgraph = ParseDisplayFile(file)
% parse display text file and store output into a Pgraph strucure array

    i=0;
    delimit=',';
    fid = fopen(file,'r');
    while feof(fid) == 0
        tline = fgetl(fid);
        if tline(1) ~= '/'
            e=strfind(tline,',');
            if length(e)==15 % si segment inclus
                i=i+1;
                [Pgraph(i).graph Pgraph(i).name Pgraph(i).plan Pgraph(i).segment Pgraph(i).type Pgraph(i).title Pgraph(i).Yminname...
                    Pgraph(i).Ymaxname Pgraph(i).Ymin Pgraph(i).Ymax Pgraph(i).Yincr Pgraph(i).Unit  Pgraph(i).fig ...
                    Pgraph(i).nplot Pgraph(i).mplot Pgraph(i).posplot]...
                    =strread(tline,'%f %s %f %s %s %s %s %s %f %f %f %s %f %f %f %f','delimiter',delimit);
            end

            if length(e)==14 % si segment non inclus
                i=i+1;
                [Pgraph(i).graph Pgraph(i).name Pgraph(i).plan Pgraph(i).type Pgraph(i).title Pgraph(i).Yminname...
                    Pgraph(i).Ymaxname Pgraph(i).Ymin Pgraph(i).Ymax Pgraph(i).Yincr Pgraph(i).Unit  Pgraph(i).fig ...
                    Pgraph(i).nplot Pgraph(i).mplot Pgraph(i).posplot]...
                    =strread(tline,'%f %s %f %s %s %s %s %f %f %f %s %f %f %f %f','delimiter',delimit);
            end
        end
    end
    fclose(fid);

    return;
end
