/affiche,nom,plan,type,titre,axe y-,y+, ymin,ymay,increment,unit,figure,subplot n,subplot m,subplot position
/
/Angles
1,PelvisAngles,1,M,Angle,Pelvis,Extension,Flexion,-20,40,20,�,1,4,2,1
1,PelvisAngles,1,M,Angle,Pelvis,Extension,Flexion,-20,40,20,�,1,4,2,2
1,FemurAngles,1,M,Angle,Thigh,Extension,Flexion,-20,80,20,�,1,4,2,3
1,HipAngles,1,M,Angle,Hip Flexion-Extension,Extension,Flexion,-20,80,20,�,1,4,2,4
1,TibiaAngles,1,M,Angle,Shank,Extension,Flexion,-80,40,20,�,1,4,2,5
1,KneeAngles,1,M,Angle,Knee Flexion-Extension,Extension,Flexion,-20,80,20,�,1,4,2,6
1,FootAngles,1,M,Angle,Foot,Plantar,Dorsiflexion,-90,30,30,�,1,4,2,7
1,AnkleAngles,1,M,Angle,Ankle Dorsi-Plantarflexion,Plantar,Dorsiflexion,-30,30,20,�,1,4,2,8