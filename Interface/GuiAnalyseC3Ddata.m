function GuiAnalyseC3Ddata(f, Extract, Cycle, Mean)
assignin('base', 'Extract', Extract);
assignin('base', 'Mean', Mean);
assignin('base', 'Cycle', Cycle);
%f = figure('Units','normalized','Position',[0.1 0.1 .75 .75]);
%f = figure('Name','Explore C3D Interface','NumberTitle','off','Menubar','none','Units','normalized','Position',[0.1 0.1 .75 .75]);
set(0,'CurrentFigure',f);

h_panel_plot = uipanel('Parent',f,'Title','Options','Fontweight','bold','Tag','plot_options_panel',...
    'Units','normalized','Position',[0.3 0.8 .75 .18],'Fontsize',8);
h_panel_sub_ses = uipanel('Parent',f,'Title','Subjects - Session','Fontweight','bold','Tag','subject_panel',...
    'Units','normalized','Position',[0.01 0.55 .25 .45],'Fontsize',8);
h_panel_cond = uipanel('Parent',f,'Title','Condition','Fontweight','bold','Tag','subject_panel',...
    'Units','normalized','Position',[0.01 0.25 .25 .3],'Fontsize',8);
h_panel_group = uipanel('Parent',f,'Title','Group','Fontweight','bold','Tag','subject_panel',...
    'Units','normalized','Position',[0.01 0.01 .25 .23],'Fontsize',8);
ax = axes('Parent',f,'Position',[0.3 0.1 .65 .68]);
mapcolor={'lines';'autumn';'winter';'summer';'parula';'gray';'hot'};
dcm_obj_cycle = datacursormode;
Leg=legend('');
time=[0:100];
H=[];
current_C3D='';
current_enf='';
current_cycle_ID=[];
current_side='';
current_cycle_n=[];
datacursormode on;
f_Sub='';

default_color_right=[0 0 1];
L_field=fieldnames(Cycle.L.Data)';
R_field=fieldnames(Cycle.R.Data)';
Data_field=[L_field R_field];
for i=1:length(Data_field)
    K{i}=Data_field{i}(2:end);
end
K=unique(K);

% display files
D=dir('D:\Programme\ExploreC3D\Param\Display\*.txt');
for i=1:length(D)
    Display_file{i}=D(i).name;
end

%Sub=unique(Extract.Subject);
%Sub=unique(Extract.Subject_Session);
Sub=unique([Cycle.L.Subject_Session Cycle.R.Subject_Session]);
%Cond=unique(Extract.Condition);
Cond=unique([Cycle.L.Condition Cycle.R.Condition]);
Group={};
%['All' S

% panel PLOT
col=[0.01 0.2 0.27 0.4 0.6 0.7 0.8];
n_c=1;
VarSelect = uicontrol('parent',h_panel_plot,'Style', 'popup','String', K,'Units','normalized','Position', [col(n_c) 0.9 0.15 0.1],'Callback', @setvar);
SubPlotOn = uicontrol('parent',h_panel_plot,'style','checkbox','units','normalized','position',[col(n_c) 0.6 0.1 0.2],'string','Subplot','value',0,'Callback', @SubplotOn);
SetSubPlot = uicontrol('parent',h_panel_plot,'Style', 'popup','String', Display_file,'Units','normalized','Position', [col(n_c) 0.5 0.15 0.1],'Callback', @setSubPlot);
n_c=n_c+1;
LeftSide = uicontrol('parent',h_panel_plot,'style','checkbox','units','normalized','position',[col(n_c) 0.7 0.2 0.25],'string','Left','value',1,'Callback', @setLeftside);
RightSide = uicontrol('parent',h_panel_plot,'style','checkbox','units','normalized','position',[col(n_c) 0.4 0.2 0.25],'string','Right','value',1,'Callback', @setRightside);
n_c=n_c+1;
Lmapcolor=uicontrol('parent',h_panel_plot,'Style', 'popup','String', mapcolor,'value',2,'Units','normalized','Position', [col(n_c) 0.75 0.1 0.2],'Callback', @setLeftColorMap);
Rmapcolor=uicontrol('parent',h_panel_plot,'Style', 'popup','String', mapcolor,'value',3,'Units','normalized','Position', [col(n_c) 0.45 0.1 0.2],'Callback', @setRightColorMap);
n_c=n_c+1;
ColorBySubject = uicontrol('parent',h_panel_plot,'style','checkbox','units','normalized','position',[col(n_c) 0.7 0.2 0.25],'string','By Subject_Session','value',0,'Callback', @setColorBySubject);
ColorByCondition = uicontrol('parent',h_panel_plot,'style','checkbox','units','normalized','position',[col(n_c) 0.4 0.2 0.25],'string','By Condition','value',0,'Callback', @setColorByCondition);
ColorByGroup = uicontrol('parent',h_panel_plot,'style','checkbox','units','normalized','position',[col(n_c) 0.1 0.2 0.25],'string','By Group','value',0,'visible','off','Callback',@setColorByGroup);
Groupcolor=uicontrol('parent',h_panel_plot,'Style', 'popup','String', mapcolor,'value',1,'Units','normalized','Position', [col(n_c)+0.1 0.12 0.1 0.2],'visible','off','Callback', @setGroupColorMap);
n_c=n_c+1;
PlotCycle = uicontrol('parent',h_panel_plot,'style','checkbox','units','normalized','position',[col(n_c) 0.7 0.2 0.25],'string','Cycle','value',1,'Callback', @setPlotCycle);
PlotMean = uicontrol('parent',h_panel_plot,'style','checkbox','units','normalized','position',[col(n_c) 0.4 0.2 0.25],'string','Mean','value',0,'Callback', @setPlotMean);
n_c=n_c+1;
Export = uicontrol('parent',h_panel_plot,'style','pushbutton','units','normalized','position',[col(n_c) 0.7 0.1 0.3],'string','Export','Callback', @Export_data);
CreateGroupe = uicontrol('parent',h_panel_plot,'style','pushbutton','units','normalized','position',[col(n_c) 0.1 0.1 0.3],'string','Group','Callback', @CreateGroup);
n_c=n_c+1;
ExportRawData = uicontrol('parent',h_panel_plot,'style','checkbox','units','normalized','position',[col(n_c) 0.7 0.1 0.25],'string','Raw Data','value',1,'Callback', @setExportRawData);
ExportDiscreteValue = uicontrol('parent',h_panel_plot,'style','checkbox','units','normalized','position',[col(n_c) 0.4 0.2 0.25],'string','DiscreteValue','value',0,'Callback', @setExportDiscreteValue);
GroupeName = uicontrol('parent',h_panel_plot,'Style','edit','Units','normalized','Position', [col(n_c) 0.1 0.1 0.25],'string','Group A');

%Subject = uicontrol('parent',h_panel_sub_ses,'style','list','max',50,'min',0,'units','normalized','position',[0 0.3 0.95 0.7],'string',['All' Sub],'Callback', @setSubject);
% Panel Subject_Session
Subject_Session = uicontrol('parent',h_panel_sub_ses,'style','list','max',50,'min',0,'units','normalized','position',[0 0 1 1],'string',['All' Sub],'Callback', @setSubject_session);

% Panel Condition
Condition = uicontrol('parent',h_panel_cond,'style','list','max',50,'min',0,'units','normalized','position',[0 0 1 1],'string',['All' Cond],'Callback', @setCondition);

% Panel Group
GroupControl = uicontrol('parent',h_panel_group,'style','list','max',50,'min',0,'units','normalized','position',[0 0 1 1],'string',Group,'Callback', @selectGroup);

% default value
%set(gca, 'ColorOrder', [0.5 0.5 0.5; 1 0 0]
S.Var=K{1};
S.Left=1;
S.Right=1;
S.Subject=[1:length(Sub)];
S.LSubSelect=[1:length(Cycle.L.Subject)];
S.RSubSelect=[1:length(Cycle.R.Subject)];
S.LCycleSelect=[1:length(Cycle.L.Subject)];
S.RCycleSelect=[1:length(Cycle.R.Subject)];
S.LSubSelectLogical=ones(length(Cycle.L.Subject),1)';
S.RSubSelectLogical=ones(length(Cycle.R.Subject),1)';
S.Condition=1:length(Cond);
S.LCondSelect=1:length(Cycle.L.Condition);
S.RCondSelect=1:length(Cycle.R.Condition);
S.LCondSelectLogical=ones(length(Cycle.L.Condition),1)';
S.RCondSelectLogical=ones(length(Cycle.R.Condition),1)';
S.LSelect=intersect(S.LSubSelect,S.LCondSelect);
S.RSelect=intersect(S.RSubSelect,S.RCondSelect);
S.LSelectColor=repmat([1 0 0],length(S.LSelect),1);
S.RSelectColor=repmat([0 0 1],length(S.RSelect),1);
S.ColorBySubject=0;
S.ColorByCondition=0;
S.ColorByGroup=0;
S.Ldefault_color=[1 0 0];
S.Rdefault_color=[0 0 1];
S.PlotCycle=1;
S.PlotMean=0;
S.MeanDataSelect='Subject';
S.Subject_Condition=1:length(Mean.Subject_Condition.Name);
S.LcolormapName=mapcolor{2};
S.RcolormapName=mapcolor{3};
S.GroupColorMap=mapcolor{1};
S.ExportRawData=1;
S.ExportDiscreteValue=0;
S.GroupN=0;
S.SubplotActive=0;
updategraph;
    function setvar(source,callbackdata) % variable selection
        data = guidata(source);
        val = source.Value;
        S.Var = source.String{val};
        updategraph;
    end

    function setLeftside(source,callbackdata) % left side selection
        S.Left = source.Value;
        setSubject_session(Subject_Session);
        setCondition(Condition);
        updategraph;
    end

    function setRightside(source,callbackdata) % Right side selection
        S.Right = source.Value;
        setSubject_session(Subject_Session);
        setCondition(Condition);
        updategraph;
    end

    function setLeftColorMap(source,callbackdata) % left side selection
        S.LcolormapName = source.String{source.Value};
        setSubject_session(Subject_Session);
        setCondition(Condition);
        updategraph;
    end

    function setRightColorMap(source,callbackdata) % left side selection
        S.RcolormapName = source.String{source.Value};
        setSubject_session(Subject_Session);
        setCondition(Condition);
        updategraph;
    end

    function setColorBySubject(source,callbackdata) % Color by Subject selection
        S.ColorBySubject=source.Value;
        if S.ColorBySubject==1 && S.ColorByCondition ==0
            S.MeanDataSelect='Subject';
        end
        if S.ColorBySubject==0 && S.ColorByCondition ==1
            S.MeanDataSelect='Condition';
        end
        if S.ColorBySubject==1 && S.ColorByCondition ==1
            S.MeanDataSelect='Subject_Condition';
        end
        if  S.PlotCycle==1
            if  S.ColorBySubject==1
                set(ColorByCondition,'value',0);
                S.ColorByCondition=0;
            end
        end
        updategraph
    end

    function CreateColorBySubject
        A=[];B=[];C=[];c=[];Sub_Sess=[];A2=[];
        S.LSelectColor=[];
        Sub_Sess=unique(Cycle.L.Subject_Session(S.LSelect));
        Cf=str2func(S.LcolormapName);
        c=colormap(Cf(length(Sub_Sess)));
        for i=1:length(Sub_Sess)
            A=strcmp(Cycle.L.Subject_Session, Sub_Sess(i));
            A2=A+S.LCondSelectLogical;
            B=find(A2==2);
            C=repmat(c(i,:),length(B),1);
            S.LSelectColor=[S.LSelectColor; C];
        end
        A=[];B=[];C=[];c=[];Sub_Sess=[];A2=[];
        S.RSelectColor=[];
        Sub_Sess=unique(Cycle.R.Subject_Session(S.RSelect));
        Cf=str2func(S.RcolormapName);
        c=colormap(Cf(length(Sub_Sess)));
        for i=1:length(Sub_Sess)
            A=strcmp(Cycle.R.Subject_Session, Sub_Sess(i));
            A2=A+S.RCondSelectLogical;
            B=find(A2==2);
            if S.ColorBySubject==1
                C=repmat(c(i,:),length(B),1);
                %             else
                %                 C=repmat(S.Rdefault_color,length(B),1);
            end
            S.RSelectColor=[S.RSelectColor; C];
        end
    end

    function setColorByCondition(source,callbackdata) % Color by Subject selection
        S.ColorByCondition=source.Value;
        if S.ColorBySubject==1 && S.ColorByCondition ==0
            S.MeanDataSelect='Subject';
        end
        if S.ColorBySubject==0 && S.ColorByCondition ==1
            S.MeanDataSelect='Condition';
        end
        if S.ColorBySubject==1 && S.ColorByCondition ==1
            S.MeanDataSelect='Subject_Condition';
        end
        if  S.PlotCycle==1
            if  S.ColorByCondition==1
                set(ColorBySubject,'value',0);
                S.ColorBySubject=0;
            end
        end
        updategraph;
    end

    function CreateColorByCondition
        A=[];B=[];C=[];c=[];Sub_Sess=[];A2=[];
        S.LSelectColor=[];
        Sub_Sess=unique(Cycle.L.Condition(S.LSelect));
        Cf=str2func(S.LcolormapName);
        c=colormap(Cf(length(Sub_Sess)));
        
        for i=1:length(S.LSelect)
            for j=1:length(Sub_Sess)
                if strcmp(Cycle.L.Condition(S.LSelect(i)), Sub_Sess(j))==1
                    S.LSelectColor(i,:)=c(j,:);
                end
            end
        end
        %         for i=1:length(Sub_Sess)
        %             A=strcmp(Cycle.L.Condition, Sub_Sess(i));
        %             A2=A+S.LSubSelectLogical;
        %             B=find(A2==2);
        %             C=repmat(c(i,:),length(B),1);
        %             S.LSelectColor=[S.LSelectColor; C];
        %         end
        A=[];B=[];C=[];c=[];Sub_Sess=[];A2=[];
        S.RSelectColor=[];
        Sub_Sess=unique(Cycle.R.Condition(S.RSelect));
        Cf=str2func(S.RcolormapName);
        c=colormap(Cf(length(Sub_Sess)));
        
        for i=1:length(S.RSelect)
            for j=1:length(Sub_Sess)
                if strcmp(Cycle.R.Condition(S.RSelect(i)), Sub_Sess(j))==1
                    S.RSelectColor(i,:)=c(j,:);
                end
            end
        end
        
        %         for i=1:length(Sub_Sess)
        %             A=strcmp(Cycle.R.Condition, Sub_Sess(i));
        %             A2=A+S.RSubSelectLogical;
        %             B=find(A2==2);
        %             C=repmat(c(i,:),length(B),1);
        %             S.RSelectColor=[S.RSelectColor; C];
        %         end
    end

    function SelectSubjectCondition
        S.Subject_Condition=[];
        Condi=unique(Cycle.L.Condition(S.LSelect));
        Sub_Sess=unique(Cycle.L.Subject_Session(S.LSelect));
        for s=1:length(Sub_Sess)
            for c=1:length(Condi)
                A=strcmp(Mean.Subject_Condition.Name, [Sub_Sess{s} '_' Condi{c}]);
                B=find(A==1);
                S.Subject_Condition=[S.Subject_Condition B];
            end
        end
        
    end

    function setSubject_session(source,callbackdata) % Subject - Session Selection
        S.Subject = source.Value;
        if source.Value(1)==1
            LSub_selec=ones(length(Cycle.L.Subject_Session),1)';
            S.Subject=[1:length(Sub)];
        else
            LSub_selec=zeros(length(Cycle.L.Subject_Session),1)';
            for i=1:length(S.Subject)
                a=strcmp(Cycle.L.Subject_Session, source.String(S.Subject(i)));
                LSub_selec=LSub_selec+a;
            end
        end
        S.LSubSelect=find(LSub_selec==1);
        S.LSubSelectLogical=LSub_selec;
        TempL=intersect(S.LSubSelect,S.LCondSelect);
        S.LSelect=intersect(S.LCycleSelect,TempL);
        
        if source.Value(1)==1
            RSub_selec=ones(length(Cycle.R.Subject_Session),1)';
        else
            RSub_selec=zeros(length(Cycle.R.Subject_Session),1)';
            for i=1:length(S.Subject)
                a=strcmp(Cycle.R.Subject_Session, source.String(S.Subject(i)));
                RSub_selec=RSub_selec+a;
            end
            S.Subject=S.Subject-1;
        end
        S.RSubSelect=find(RSub_selec==1);
        S.RSubSelectLogical=RSub_selec;
        TempR=intersect(S.RSubSelect,S.RCondSelect);
         S.RSelect=intersect(S.RCycleSelect,TempR);
        ColorByGroup.Value=0;
        GroupControl.Value=[];
        S.ColorByGroup=0;
        updategraph;
    end

    function setCondition(source,callbackdata) % Condition Selection
        S.Condition = source.Value;
        if source.Value(1)==1
            LCond_selec=ones(length(Cycle.L.Condition),1)';
            S.Condition=[1:length(Cond)];
        else
            LCond_selec=zeros(length(Cycle.L.Condition),1)';
            for i=1:length(S.Condition)
                a=strcmp(Cycle.L.Condition, source.String(S.Condition(i)));
                LCond_selec=LCond_selec+a;
            end
        end
        S.LCondSelect=find(LCond_selec==1);
        S.LCondSelectLogical=LCond_selec;
        S.LSelect=intersect(S.LSubSelect,S.LCondSelect);
        
        if source.Value(1)==1
            RCond_selec=ones(length(Cycle.R.Condition),1)';
        else
            RCond_selec=zeros(length(Cycle.R.Condition),1)';
            for i=1:length(S.Condition)
                a=strcmp(Cycle.R.Condition, source.String(S.Condition(i)));
                RCond_selec=RCond_selec+a;
            end
            S.Condition =S.Condition-1;
        end
        S.RCondSelect=find(RCond_selec==1);
        S.RCondSelectLogical=RCond_selec;
        S.RSelect=intersect(S.RSubSelect,S.RCondSelect);
        ColorByGroup.Value=0;
        S.ColorByGroup=0;
        GroupControl.Value=[];
        updategraph
    end

    function setPlotCycle(source,callbackdata) % update the figure
        S.PlotCycle=source.Value;
        if  S.PlotCycle==1
            set(PlotMean,'value',0);
            S.PlotMean=0;
            CreateGroupe.Enable='on';
        end
        updategraph
    end

    function setPlotMean(source,callbackdata) % update the figure
        S.PlotMean=source.Value;
        if  S.PlotMean==1
            set(PlotCycle,'value',0);
            S.PlotCycle=0;
            CreateGroupe.Enable='off';
        end
        updategraph
    end

    function txt = myupdatefcn(empt,event_obj)
        pos = get(event_obj,'Position');
        tar = get(event_obj,'Target');
        nam = get(tar,'DisplayName');
        [Su Ses File Enf Folder Side cyc start stop n]  =strread(nam,'%s %s %s %s %s %s %s %s %s %s','delimiter',',');
        txt = {['Subject: ' char(Su)],['Session: ' char(Ses)],['C3D file: ' char(File)],['EnfQtf file: ' char(Enf)],['Folder: ' char(Folder)], ['Side: ' char(Side)], ['Cycle: ' char(cyc)],  ['Start_Frame: ' char(start)],...
            ['End_Frame: ' char(stop)],['Time: ',num2str(pos(1))],['Value: ',num2str(pos(2))],['Cycle ID: ' char(n)]};
        current_C3D=[char(Folder) '\' char(File) '.c3d'];
        current_enf=[char(Folder) '\' char(Enf)];
        current_cycle_ID=str2num(char(n));
        current_side=char(Side);
        current_cycle_n=str2num(char(cyc));
    end
    function txt = myupdatefcn_mean(empt,event_obj)
        pos = get(event_obj,'Position');
        tar = get(event_obj,'Target');
        nam = get(tar,'DisplayName');
        %[Su Ses File Folder Side cyc start stop]  =strread(nam,'%s %s %s %s %s %s %s %s ','delimiter',',');
        txt = {['Name: ' char(nam)],['Time: ',num2str(pos(1))],['Value: ',num2str(pos(2))]};
    end

%% PLOT
    function updategraph % update the figure
        % if S.Subplot==0
        Leg=legend('off');
        
        % Cycles
        if S.PlotCycle==1
            S.LData=[];S.LID=[];S.LCurrentVar='';
            if S.Left==1
                S.LData=Cycle.L.Data.(['L' S.Var])(S.LSelect,:);
                S.LID=Cycle.L.ID(S.LSelect)';
                S.LCurrentVar=cellstr(repmat(['L' S.Var],length(S.LID),1));
                
            else
                S.LSelect=[];
            end
            
            S.RData=[];S.RID=[];S.RCurrentVar='';
            if S.Right==1
                S.RData=Cycle.R.Data.(['R' S.Var])(S.RSelect,:);
                S.RID=Cycle.R.ID(S.RSelect)';
                S.RCurrentVar=cellstr(repmat(['R' S.Var],length(S.RID),1));
            else
                S.RSelect=[];
            end
            
            if S.ColorBySubject==1 && S.ColorByCondition==0
                CreateColorBySubject;
            end
            if S.ColorBySubject==0 && S.ColorByCondition==1
                CreateColorByCondition;
            end
            if S.ColorBySubject==0 && S.ColorByCondition==0
                S.LSelectColor=repmat(S.Ldefault_color,length(S.LSelect),1);
                S.RSelectColor=repmat(S.Rdefault_color,length(S.RSelect),1);
            end
            S.Data=[];
            S.Data=[ S.LData;  S.RData];
            S.DataColor=[S.LSelectColor; S.RSelectColor];
            S.DataID=[S.LID; S.RID];
            S.CurrentVar=[S.LCurrentVar; S.RCurrentVar];
            
            
            if S.ColorByGroup==1
                S.Data=[];S.DataID=[];S.DataGroup=[]; S.DataColor=[];S.CurrentVar='';S.DataGroupName='';
                Cf=str2func(S.GroupColorMap);
                c=colormap(Cf(length(S.GroupSelection)));
                cn=0;
                for gn=S.GroupSelection
                    cn=cn+1;
                    S.Data=[S.Data;S.Group(gn).Data];
                    S.DataID=[S.DataID;S.Group(gn).DataID];
                    S.CurrentVar=[S.CurrentVar;S.Group(gn).CurrentVar];
                    S.DataGroup=[S.DataGroup;repmat(gn,length(S.Group(gn).DataID),1)];
                    S.DataGroupName=[S.DataGroupName;cellstr(repmat(S.Group(gn).Name,length(S.Group(gn).DataID),1))];
                    C=repmat(c(cn,:),length(S.Group(gn).DataID),1);
                    S.DataColor=[S.DataColor; C];
                end
            end
            
            if length(S.Data)>1
                set(gca, 'ColorOrder',  S.DataColor, 'NextPlot', 'replacechildren');
                H=plot(time,S.Data);hold off
                set(H,{'DisplayName'},S.DataID);
            else
                cla;
            end
            dcm_obj_cycle = datacursormode;
            set(dcm_obj_cycle,'DisplayStyle','datatip','SnapToDataVertex','on','Enable','on');
            set(dcm_obj_cycle,'UpdateFcn',@myupdatefcn);
            c = uicontextmenu;
            m1 = uimenu(c,'Label','Open C3D','Callback',@OpenC3DMokka);
            m2 = uimenu(c,'Label','Open QTF/ENF','Callback',@OpenENF_QTF);
            m3 = uimenu(c,'Label','Exclude Cycle','Callback',@Exclude_Cycle);
            set(dcm_obj_cycle,'UIContextMenu',c);
            % Set c to be the plot line's UIContextMenu
            hold off
        else
            cla;
        end
        
        % Mean
        if S.PlotMean==1 % Mean
            switch S.ColorBySubject+S.ColorByCondition
                case 0 % no selection
                    cla;
                case 1 % selection cond ou sub
                    PlottingMean;
                case 2 % selection cond + sub
                    SelectSubjectCondition;
                    PlottingMean;
            end
            if S.ColorByGroup==1
                % TO DO
            end
        end
        % end
        if S.SubplotActive==1 && S.PlotMean==0
            Pgraph = ParseDisplayFile(S.Subplotfile);
            if isempty(f_Sub)==1
                f_Sub=figure('Name','Explore C3D Subplot','Color',[1 1 1],'NumberTitle','off','Menubar','none','Units','normalized','Position',[0.1 0.1 .75 .75]);
                % MENUS
                h_figure_menu = uimenu('Label','Export');
                uimenu('Parent',h_figure_menu,'Label','Raw Data','Callback',{@ExportDataFigureSubPlot});
            else
                if isvalid(f_Sub)==1
                    set(0,'CurrentFigure',f_Sub);
                    clf
                else
                    f_Sub=figure('Name','Explore C3D Subplot','Color',[1 1 1],'NumberTitle','off','Menubar','none','Units','normalized','Position',[0.1 0.1 .75 .75]);
                    % MENUS
                       h_figure_menu = uimenu('Label','Export');
                     uimenu('Parent',h_figure_menu,'Label','Raw Data','Callback',{@ExportDataFigureSubPlot});
                end
            end
            for Ps=1:length(Pgraph)
                if Pgraph(Ps).graph==1
                    if Pgraph(Ps).fig==1
                        
                        S.LData=[];S.LID=[];
                        if S.Left==1
                            Var=['L' char(Pgraph(Ps).name) num2str(Pgraph(Ps).plan)];
                            if isfield(Cycle.L.Data,Var)
                                S.LData=Cycle.L.Data.(Var)(S.LSelect,:);
                                S.Subplot(Ps).LData=S.LData;
                                
                            else
                                S.LData=[];
                            end
                            S.LID=Cycle.L.ID(S.LSelect)';
                            S.LCurrentVar=cellstr(repmat(Var,length(S.LID),1));
                            
                        else
                            S.LSelect=[];
                        end
                        
                        S.RData=[];S.RID=[];
                        if S.Right==1
                            Var=['R' char(Pgraph(Ps).name) num2str(Pgraph(Ps).plan)];
                            if isfield(Cycle.R.Data,Var)
                                S.RData=Cycle.R.Data.(Var)(S.RSelect,:);
                                S.Subplot(Ps).RData=S.RData;
                            else
                                S.RData=[];
                            end
                            S.RID=Cycle.R.ID(S.RSelect)';
                            S.RCurrentVar=cellstr(repmat(Var,length(S.RID),1));
                        else
                            S.RSelect=[];
                        end
                        S.Data=[ S.LData;  S.RData];
                        if length(S.Data)>1
                            
                            axe(Ps)=subplot(Pgraph(Ps).nplot,Pgraph(Ps).mplot,Pgraph(Ps).posplot);
                            set(gca, 'ColorOrder',  S.DataColor, 'NextPlot', 'replacechildren');
                            Subp(Ps).P=plot(time,S.Data);hold off
                            set(Subp(Ps).P,{'DisplayName'},S.DataID);
                            title(char(Pgraph(Ps).title),'FontWeight','bold');
                            ylabel(strcat(Pgraph(Ps).Yminname,'/',Pgraph(Ps).Ymaxname,' (',Pgraph(Ps).Unit,')'),'FontSize',8);
                            ylim([Pgraph(Ps).Ymin Pgraph(Ps).Ymax]);
                            set(axe(Ps),'YTick',Pgraph(Ps).Ymin:Pgraph(Ps).Yincr:Pgraph(Ps).Ymax);
                            set(axe(Ps),'XTick',[0:20:100],'NextPlot','add','Parent',f_Sub);
                            xlim([0 100]);
                        else
                            cla;
                        end
                        dcm_obj_cycle = datacursormode;
                        set(dcm_obj_cycle,'DisplayStyle','datatip','SnapToDataVertex','on','Enable','on');
                        set(dcm_obj_cycle,'UpdateFcn',@myupdatefcn);
                        c = uicontextmenu;
                        m1 = uimenu(c,'Label','Open C3D','Callback',@OpenC3DMokka);
                        m2 = uimenu(c,'Label','Open QTF/ENF','Callback',@OpenENF_QTF);
                        m3 = uimenu(c,'Label','Exclude Cycle','Callback',@Exclude_Cycle);
                        set(dcm_obj_cycle,'UIContextMenu',c);
                        % Set c to be the plot line's UIContextMenu
                        hold off
                    end
                end
            end
            
        end
    end

    function PlottingMean
        cla; S.LData=[];S.RData=[];L=[];R=[];
        if S.Left==1
            S.LData=[];
            S.LData=Mean.(S.MeanDataSelect).L.Data.(['L' S.Var])(S.(S.MeanDataSelect),:);
            Cf=str2func(S.LcolormapName);
            if length(S.LData)>1
                S.LSelectColor=colormap(Cf(size(S.LData,1)));
                L=strcat('L_',Mean.(S.MeanDataSelect).Name(S.(S.MeanDataSelect)));
            else  S.LSelectColor=[];
            end
        else   S.LSelectColor=[];
        end
        if S.Right==1
            S.RData=Mean.(S.MeanDataSelect).R.Data.(['R' S.Var])(S.(S.MeanDataSelect),:);
            Cf=str2func(S.RcolormapName);
            if length(S.RData)>1
                S.RSelectColor=colormap(Cf(size(S.RData,1)));
                R=strcat('R_',Mean.(S.MeanDataSelect).Name(S.(S.MeanDataSelect)));
            else  S.RSelectColor=[];
            end
        else   S.RSelectColor=[];
        end
        S.Data=[];
        S.Data=[ S.LData;  S.RData];S.DataColor=[];
        if length(S.Data)>1
            S.DataColor=[S.LSelectColor; S.RSelectColor];
            %S.DataID=[S.LID; S.RID];
            set(gca, 'ColorOrder',  S.DataColor, 'NextPlot', 'replacechildren');
            H=plot(time,S.Data,'linewidth',4);
            Legend=[L,R];
            if length(L)<30
                Leg=legend(Legend,'Interpreter','none','Location','bestoutside');
            end
            hold off
            dcm_obj_mean = datacursormode;
            set(dcm_obj_mean,'DisplayStyle','datatip','SnapToDataVertex','off','Enable','on');
            set(dcm_obj_mean,'UpdateFcn',@myupdatefcn_mean);
        else
            cla;
        end
        if S.SubplotActive==1
            Pgraph = ParseDisplayFile(S.Subplotfile);
            if isempty(f_Sub)==1
                f_Sub=figure('Name','Explore C3D Subplot','Color',[1 1 1],'NumberTitle','off','Menubar','none','Units','normalized','Position',[0.1 0.1 .75 .75]);
            else
                if isvalid(f_Sub)==1
                    set(0,'CurrentFigure',f_Sub);
                    clf
                else
                    f_Sub=figure('Name','Explore C3D Subplot','Color',[1 1 1],'NumberTitle','off','Menubar','none','Units','normalized','Position',[0.1 0.1 .75 .75]);
                end
            end
            for Ps=1:length(Pgraph)
                if Pgraph(Ps).graph==1
                    if Pgraph(Ps).fig==1
                        
                        S.LData=[];S.LID=[];
                        if S.Left==1
                            Var=['L' char(Pgraph(Ps).name) num2str(Pgraph(Ps).plan)];
                            if isfield(Mean.(S.MeanDataSelect).L.Data,Var)
                                S.LData=Mean.(S.MeanDataSelect).L.Data.(Var)(S.(S.MeanDataSelect),:);
                            else
                                S.LData=[];
                            end
                        else
                            S.LSelect=[];
                        end
                        
                        S.RData=[];S.RID=[];
                        if S.Right==1
                            Var=['R' char(Pgraph(Ps).name) num2str(Pgraph(Ps).plan)];
                            if isfield(Mean.(S.MeanDataSelect).R.Data,Var)
                                S.RData=Mean.(S.MeanDataSelect).R.Data.(Var)(S.(S.MeanDataSelect),:);
                            else
                                S.RData=[];
                            end
                        else
                            S.RSelect=[];
                        end
                        S.Data=[ S.LData;  S.RData];
                        if length(S.Data)>1
                            axe(Ps)=subplot(Pgraph(Ps).nplot,Pgraph(Ps).mplot,Pgraph(Ps).posplot);
                            set(gca, 'ColorOrder',  S.DataColor, 'NextPlot', 'replacechildren');
                            Subp(Ps).P=plot(time,S.Data,'linewidth',4);hold off
                            % set(Subp(Ps).P,{'DisplayName'},S.DataID);
                            title(char(Pgraph(Ps).title),'FontWeight','bold');
                            ylabel(strcat(Pgraph(Ps).Yminname,'/',Pgraph(Ps).Ymaxname,' (',Pgraph(Ps).Unit,')'),'FontSize',8);
                            ylim([Pgraph(Ps).Ymin Pgraph(Ps).Ymax]);
                            set(axe(Ps),'YTick',Pgraph(Ps).Ymin:Pgraph(Ps).Yincr:Pgraph(Ps).Ymax);
                            set(axe(Ps),'XTick',[0:20:100],'NextPlot','add','Parent',f_Sub);
                            xlim([0 100]);
                            if length(L)<30
                                %  Leg=legend(Legend,'Interpreter','none','Location','bestoutside');
                            end
                            hold off
                            set(dcm_obj_mean,'DisplayStyle','datatip','SnapToDataVertex','off','Enable','on');
                            set(dcm_obj_mean,'UpdateFcn',@myupdatefcn_mean);
                        else
                            cla;
                        end
                        
                    end
                end
            end
            
        end
    end
    function OpenC3DMokka(source,callbackdata)
        system(['"D:\Programme\Mokka64\Mokka.exe" -' current_C3D]);
    end
    function OpenENF_QTF(source,callbackdata)
        system(current_enf);
        %system(['nodepad "',current_enf,'"']);
    end
    function Exclude_Cycle(source,callbackdata)
        Lpasse_exclude_cycle=0;
        Rpasse_exclude_cycle=0;
        if isempty(current_cycle_ID)==0
            if strcmp(current_side,'Left')
                S.LCycleSelect(S.LCycleSelect==current_cycle_ID)=[];
                TempL=intersect(S.LSubSelect,S.LCondSelect);
                S.LSelect=intersect(S.LCycleSelect,TempL);
                Lpasse_exclude_cycle=1;
                
            end
            if strcmp(current_side,'Right')
                S.RCycleSelect(S.RCycleSelect==current_cycle_ID)=0;
                TempR=intersect(S.RSubSelect,S.RCondSelect);
                S.RSelect=intersect(S.RCycleSelect,TempR);
                Rpasse_exclude_cycle=1;
            end
            % write in the qtf/enf file
            if (Rpasse_exclude_cycle+Lpasse_exclude_cycle)==1
                if exist(current_enf)
                    fid = fopen(current_enf,'r'); % open qtf/enf
                    data = textscan(fid, '%s', 'delimiter', '\r');
                    qtf_data = data{1};
                    fclose(fid);
                    if Lpasse_exclude_cycle==1;
                        left_idx = find(~cellfun(@isempty,strfind(qtf_data,'LEFT_CYCLE')));
                        left_vec = strsplit(qtf_data{left_idx},'=');
                        left_vec{2}(current_cycle_n*2-1) = num2str(0);
                        qtf_data{left_idx} = sprintf('%s=%s', left_vec{1},left_vec{2});
                    end
                    if Rpasse_exclude_cycle==1;
                        right_idx = find(~cellfun(@isempty,strfind(qtf_data,'RIGHT_CYCLE')));
                        right_vec = strsplit(qtf_data{right_idx},'=');
                        right_vec{2}(current_cycle_n*2-1) = num2str(0);
                        qtf_data{right_idx} = sprintf('%s=%s', right_vec{1},right_vec{2});
                    end
                    str2save = '';
                    for qq = 1:length(qtf_data)
                        str2save = sprintf('%s%s\r', str2save,qtf_data{qq});
                    end
                    fid = fopen(current_enf,'w+');
                    fprintf(fid,str2save);
                    fclose(fid);
                end
            end
            updategraph
        end
        %system(['nodepad "',current_enf,'"']);
    end
%% EXPORT
    function Export_data(~,callbackdata)
        t=datestr(now,'yyyy-mm-dd_HHMM');
        saveexportfolder=GetParameterExploreC3DPref('PathExport');
        nameexport='mydata';
        if S.PlotMean==1
            L='';
            if S.Left==1
                L=strcat('L_',Mean.(S.MeanDataSelect).Name(S.(S.MeanDataSelect)));
            end
            R='';
            if S.Right==1
                R=strcat('R_',Mean.(S.MeanDataSelect).Name(S.(S.MeanDataSelect)));
            end
            id_table=[L R]';
        else
            
        end
        if S.PlotCycle==1
            id_table=horzcat(S.DataID,S.CurrentVar); % for cycle
        end
        file2saveExport=[saveexportfolder '\' nameexport '_' t];
        if S.ExportRawData==1
            xlswrite([file2saveExport,'.xlsx'],id_table,'Raw');
            xlswrite([file2saveExport,'.xlsx'],S.Data,'Raw','C1');
            if ColorByGroup.Value==1
                id_table=horzcat(S.DataID,S.CurrentVar,S.DataGroupName);
                xlswrite([file2saveExport,'.xlsx'],id_table,'Raw');
                xlswrite([file2saveExport,'.xlsx'],S.Data,'Raw','D1');
            end
        end
        if S.ExportDiscreteValue==1
            VarDiscrete={'Name,Session,C3D_File,Enf_file,Folder,Side,Cycle,StartFrame,EndFrame','Var','V_Max','T_Max','V_Min','T_Min','Range','Mean'};
            [TableDiscrete(:,1) TableDiscrete(:,2)]=max(S.Data');
            [TableDiscrete(:,3) TableDiscrete(:,4)]=min(S.Data');
            TableDiscrete(:,5)=TableDiscrete(:,1)-TableDiscrete(:,3);
            TableDiscrete(:,6)=mean(S.Data');
            xlswrite([file2saveExport,'.xlsx'],VarDiscrete,'Discrete');
            xlswrite([file2saveExport,'.xlsx'],id_table,'Discrete','A2');
            xlswrite([file2saveExport,'.xlsx'],TableDiscrete,'Discrete','C2');
            if ColorByGroup.Value==1
                id_table=horzcat(S.DataID,S.CurrentVar,S.DataGroupName);
                VarDiscrete={'Name,Session,File,Folder,Side,Cycle,StartFrame,EndFrame','Var','Group','V_Max','T_Max','V_Min','T_Min','Range','Mean'};
                xlswrite([file2saveExport,'.xlsx'],VarDiscrete,'Discrete');
                xlswrite([file2saveExport,'.xlsx'],id_table,'Discrete','A2');
                xlswrite([file2saveExport,'.xlsx'],TableDiscrete,'Discrete','D2');
            end
        end
        system(['explorer "',saveexportfolder,'"']);
    end
    function setExportRawData(source,callbackdata)
        S.ExportRawData=source.Value;
    end
    function setExportDiscreteValue(source,callbackdata)
        S.ExportDiscreteValue=source.Value;
    end
    function ExportDataFigureSubPlot(~,callbackdata)
        t=datestr(now,'yyyy-mm-dd_HHMM');
        saveexportfolder=GetParameterExploreC3DPref('PathExport');
        nameexport='mydata';
        file2saveExport=[saveexportfolder '\' nameexport '_' t];
        xlswrite([file2saveExport,'.xlsx'],S.Data,'Raw','B1');
        system(['explorer "',saveexportfolder,'"']);
        for Ps=1:length(S.Subplot)
            S.Subplot(Ps).RData
            xlswrite([file2saveExport,'.xlsx'],S.Subplot(Ps).RData,num2str(Ps),'B1');
        end
    end

%% GROUp
    function CreateGroup(source,callbackdata)
        ColorByGroup.Visible='on';
        Groupcolor.Visible='on';
        S.GroupN=S.GroupN+1;
        S.Group(S.GroupN).Name=GroupeName.String;
        %         S.Group(S.GroupN).LData=S.LData;
        %         S.Group(S.GroupN).LID=S.LID;
        %         S.Group(S.GroupN).RData=S.RData;
        %         S.Group(S.GroupN).RID=S.RID;
        %         S.Group(S.GroupN).Data=S.Data;
        S.Group(S.GroupN).Data=S.Data;
        S.Group(S.GroupN).DataID=S.DataID;
        S.Group(S.GroupN).CurrentVar=S.CurrentVar;
        Group=[Group GroupeName.String];
        GroupControl.String=Group;
        S.GroupSelection=S.GroupN;
        GroupControl.Value=S.GroupSelection;
        ColorByGroup.Value=1;
        S.ColorByGroup=1;
        updategraph
    end

    function selectGroup(source,callbackdata) % Condition Selection
        S.GroupSelection = source.Value;
        GroupControl.Value=S.GroupSelection;
        ColorByGroup.Value=1;
        S.ColorByGroup=1;
        Subject_Session.Value=[];
        Condition.Value=[];
        ColorBySubject.Value=0;
        ColorByCondition.Value=0;
        updategraph;
        
    end

    function setColorByGroup(source,callbackdata) % Condition Selection
        S.ColorByGroup = source.Value;
        ColorByGroup.Value=1;
        S.ColorByGroup=1;
        Subject_Session.Value=[];
        Condition.Value=[];
        ColorBySubject.Value=0;
        ColorByCondition.Value=0;
        updategraph;
    end

    function setGroupColorMap(source,callbackdata) % left side selection
        S.GroupColorMap= source.String{source.Value};
        updategraph;
    end

    function CreateColorByGroup
    end

    function SubplotOn(source,callbackdata)
        S.SubplotActive = source.Value;
        S.Subplotfile=SetSubPlot.String{SetSubPlot.Value};
        updategraph;
    end

    function setSubPlot(source,callbackdata)
        S.SubplotActive = SubPlotOn.Value;
        S.Subplotfile=source.String{source.Value};
        updategraph;
    end



end
