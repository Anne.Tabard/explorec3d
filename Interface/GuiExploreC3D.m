function [Extract, Cycle, Mean]=GuiExploreC3D
% FIGURE
warning off
f = figure('Name','Explore C3D Interface','NumberTitle','off','Menubar','none','Units','normalized','Position',[0.1 0.1 .75 .75]);
% fh=figure('Name','Data Processing Interface',,'Units','normalized','Position',[0 0 .55 .9],'Visible','off',...
%     'CloseRequestFcn',{@back_to_menu_Callback},'Tag','data_processing_window');
%movegui(fh,'center');

% MENUS
h_files_menu = uimenu('Label','File');
uimenu('Parent',h_files_menu,'Label','Extract C3D files','Callback',{@ExtractC3DFiles});
uimenu('Parent',h_files_menu,'Label','Read C3D files list','Callback',{@ReadC3DFiles});
uimenu('Parent',h_files_menu,'Label','Load Data','Callback',{@LoadExtractData});

Extract=[];
Cycle=[];
Mean=[];
def_save_data=GetParameterExploreC3DPref('PathDataSave');
    function LoadExtractData(~,~)
        [FileName,PathName] = uigetfile([def_save_data '\*.mat'],'Select the Extract data to load');
        A=load([PathName FileName]);
        Extract=A.Extract;
        [Mean Cycle]=Extract2Cycle(Extract);
        GuiAnalyseC3Ddata(f,Extract, Cycle, Mean);
    end

    function ExtractC3DFiles(~,~)
        [filter folder2explore Paramfile Computefile S.folder2save S.file2save]=GuiSetExtractParameter;
        %         %[FileName,PathName] = uigetfile('*.mat','Select the Extract data to load');
        Param=ParseC3Dparameter(Paramfile);
        Compute=ParseComputeFile(Computefile);
        folder2explore=[folder2explore '\'];
        Extract=Extract_C3D_Data(folder2explore, filter, Param, Compute, S);
        %save([folder2save '\' file2save],'Extract');
        [Mean Cycle]=Extract2Cycle(Extract);
        GuiAnalyseC3Ddata(f,Extract, Cycle, Mean);
    end

    function ReadC3DFiles(~,~)
        [List Paramfile Computefile S.folder2save S.file2save]=GuiSetExtractParameterReadC3DList;
        %         %[FileName,PathName] = uigetfile('*.mat','Select the Extract data to load');
        Param=ParseC3Dparameter(Paramfile);
        Compute=ParseComputeFile(Computefile);
       
        %folder2explore=[folder2explore '\'];
       % Extract=Extract_C3D_Data(folder2explore, filter, Param, Compute, S);
        P = ParseLibrary(List);
         Extract=Read_C3D_Data(P,Param, Compute, S);
        %save([folder2save '\' file2save],'Extract');
        [Mean Cycle]=Extract2Cycle(Extract);
        GuiAnalyseC3Ddata(f,Extract, Cycle, Mean);
    end

end