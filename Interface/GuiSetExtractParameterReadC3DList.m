function [file2read ParamfileF ComputefileF folder2save file2save]=GuiSetExtractParameterReadC3DList

file2read=[];
folder2save=[];
file2save=[];
def_path=GetParameterExploreC3DPref('PathDataRead');
def_save=GetParameterExploreC3DPref('PathDataSave');
def_filter=GetParameterExploreC3DPref('Filter');
def_param=GetParameterExploreC3DPref('PathParam');
def_compute=GetParameterExploreC3DPref('PathCompute');
PredDataSave=GetParameterExploreC3DPref('PredDataSave');
ParamfileF='';
ComputefileF='';
%def_param_file='ParamKinematic.txt';
def_save_name='MyData';
%def_filter='*.c3d';
g = figure('Name','Select C3D files list to read and options','NumberTitle','off','Menubar','none','Units','normalized','Position',[0.3 0.3 .3 .3]);
Pathtext = uicontrol('parent',g,'Style','text','Units','normalized','Position', [0.03 0.88 0.5 0.1],'string','C3D Files list to read');
Pathdef = uicontrol('parent',g,'Style','edit','Units','normalized','Position', [0.03 0.8 0.75 0.1],'string',def_path);
PathPushButton= uicontrol('parent',g,'Style','pushbutton','Units','normalized','Position', [0.8 0.8 0.2 0.1],'string','Set File','Callback', @SetFolder);

%C3DFilterText = uicontrol('parent',g,'Style','text','Units','normalized','Position', [0.03 0.65 0.65 0.1],'string','Filter of C3D - defaut *.c3d - example *GBNNN*.c3d');
%C3DFilterDef = uicontrol('parent',g,'Style','edit','Units','normalized','Position', [0.03 0.58 0.3 0.1],'string',def_filter);

p_f=dir([def_param '\*.txt']);
c_f=dir([def_compute '\*.txt']);

ParamSelectionText = uicontrol('parent',g,'Style','text','Units','normalized','Position', [0.03 0.45 0.4 0.1],'string','Parameters to extract in the C3D files');
ParamSelection = uicontrol('parent',g,'style','popupmenu','units','normalized','position',[0.03 0.38 0.4 0.1],'string',{char(p_f.name)},'Callback', @GetParamFile);

ComputeSelectionText = uicontrol('parent',g,'Style','text','Units','normalized','Position', [0.5 0.45 0.4 0.1],'string','Computation to do');
ComputeSelection = uicontrol('parent',g,'style','popupmenu','units','normalized','position',[0.5 0.38 0.4 0.1],'string',{char(c_f.name)},'Callback', @GetComputeFile);

PathTextSave = uicontrol('parent',g,'Style','text','Units','normalized','Position', [0.03 0.26 0.2 0.1],'string','Folder to save Extract Data');
PathdefSave = uicontrol('parent',g,'Style','edit','Units','normalized','Position', [0.03 0.20 0.75 0.1],'string',def_save);
PathPushButtonSave= uicontrol('parent',g,'Style','pushbutton','Units','normalized','Position', [0.8 0.20 0.2 0.1],'string','Set Folder','Callback', @SetFolderSave);

NameSaveFileText = uicontrol('parent',g,'Style','text','Units','normalized','Position', [0.03 0.07 0.3 0.1],'string','Name of the saved Data');
NameSaveFileDef = uicontrol('parent',g,'Style','edit','Units','normalized','Position', [0.03 0.01 0.2 0.1],'string',def_save_name);
setnamedatasaved;

OKPushButton= uicontrol('parent',g,'Style','pushbutton','Units','normalized','Position', [0.8 0.01 0.1 0.1],'string','OK','Callback', @OKContinue);
   
    function SetFolder(~,~)
        [file2read,path2read] = uigetfile('*.txt','Select C3D Files list to read');
        Pathdef.String=[path2read file2read];
        set(NameSaveFileDef,'string',def_save_name);
        setnamedatasaved
    end

    function SetFolderSave(~,~)
        folder2save = uigetdir(def_save,'Select C3D Files list to read');
        PathdefSave.String=folder2save;
    end

    function setnamedatasaved
        aa=findstr(Pathdef.String,'\');
        if length(aa)>1
            def_save_name=[PredDataSave Pathdef.String(aa(end)+1:end)];
        else
            def_save_name=[PredDataSave 'MyData'];
        end
        NameSaveFileDef.String=def_save_name;
    end

    function GetParamFile(source,callbackdata)
         Paramfile = source.String{source.Value};
         ParamfileF=[def_param '\' Paramfile];
    end

    function GetComputeFile(source,callbackdata)
         Computefile = source.String{source.Value};
         ComputefileF=[def_compute '\' Computefile];
    end

    function OKContinue(~,~)
        %filter=C3DFilterDef.String;
        file2read=Pathdef.String;
        folder2save=PathdefSave.String
        file2save=NameSaveFileDef.String;
        ParamfileF=[def_param '\' ParamSelection.String{ParamSelection.Value}];
        ComputefileF=[def_compute '\' ComputeSelection.String{ComputeSelection.Value}];
        uiresume(gcbf);
        close(g);
    end
    
uiwait(gcf);
end